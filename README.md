# AirportSystem
simple airport system using sql database
## form1
form(login for the admin or normal user go to passenger tab

![Alt text](pics/form1.png)

## flight(for admin accessing)
<br>
update the flight and its dates and changes

![Alt text](pics/flight.png)

## passenger
<br>
enter your info and image of you

![Alt text](pics/passenger.png)

## ticket
<br>
select a flight and after selection you can print it

![Alt text](pics/ticket.png)


## ticket Output
<br>

![Alt text](pics/ticketOutput.png)
